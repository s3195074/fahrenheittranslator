package nl.utwente.di.fahrenheitTranslator;

import java.io.*;
import jakarta.servlet.*;
import jakarta.servlet.http.*;

/** Example of a Servlet that gets an ISBN number and returns the book price
 */

public class FahrenheitTranslator extends HttpServlet {
 	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

    public void init() throws ServletException {
    }
	
  public void doGet(HttpServletRequest request,
                    HttpServletResponse response)
      throws ServletException, IOException {

    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    String title = "Fahrenheit Calculator";

    double celsius;
    try {
        celsius = Double.parseDouble(request.getParameter("celsius"));
    } catch (NumberFormatException e) {
        out.println("<!DOCTYPE HTML>\n" +
                            "<HTML>\n" +
                            "<HEAD><TITLE>" + title + "</TITLE>" +
                            "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                            "</HEAD>\n" +
                            "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                            "<H1>Invalid request</H1>\n" +
                            "</BODY></HTML>");
        return;
    }

    // Done with string concatenation only for the demo
    // Not expected to be done like this in the project
    out.println("<!DOCTYPE HTML>\n" +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                		"</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +              
                "  <P>Celsius temperature: " +
                   celsius + "\n" +
                "  <P>Fahrenheit temperature: " +
                   celsiusToFahrenheit(celsius) +
                "</BODY></HTML>");
  }

  private double celsiusToFahrenheit(double celsius) {
        return celsius * 9/5 + 32;
  }

}
